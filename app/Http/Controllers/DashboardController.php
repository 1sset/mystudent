<?php

namespace App\Http\Controllers;
use \Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $count_students = count(\App\Student::get());
        $count_workers = count(\App\Worker::get());
        $name = \App\Dashboardinfo::first();
        $student_stats = json_decode(\App\Student::get(['is_present', 'visits_per_week', 'visits_per_month']));
        $workers_stats = json_decode(\App\Worker::get(['is_present', 'visits_per_week', 'visits_per_month']));
        $stats = array_merge($student_stats, $workers_stats);
        $stats_per_day = 0; 
        $stats_per_week = 0;
        $stats_per_month = 0;
        foreach ($stats as $stat)
        {
            $stats_per_day = $stats_per_day + $stat->is_present;
            $stats_per_week = $stats_per_week + $stat->visits_per_week;
            $stats_per_month = $stats_per_month + $stat->visits_per_month;
        }
        return view('welcome', compact('count_students', 'count_workers', 'name', 'stats_per_day', 'stats_per_month', 'stats_per_week'));
        
    }
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
