<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        $info = \App\Dashboardinfo::first();
        $user = \App\User::first();
        return view('settings/index', compact('user', 'info'));
    }
    public function update(Request $request)
    {
        if ($request->method('post')) {

            /* 
            |---------------------------------------------------------------------------
            | Валидация: Обновление
            |---------------------------------------------------------------------------
            |
            */

            $rules =[
                'name' => 'required|max:191|min:2',
                'email' => 'required|email|max:191',
                'password' => 'required|min:8|max:191',
                
            ];
            $messages = [
                'name.required' => 'Вы не заполнили поле: Название учебного заведения',
                'name.min' => 'Название должно стостоять не менее, чем из 2х символов',

                'email.email' => 'Не верный тип почты',
                'email.required' => 'Вы не заполнили поле: Почта',

                'password.required' => 'Вы не заполнили поле: Пароль от почты',
                'password.min' => 'Пароль должен состоять не менее чем из 8 символов',
            ];
            $this->validate($request,$rules, $messages);
        }
        $info = \App\Dashboardinfo::first();
        $user_info = \App\User::first();
        $info->name = $request->name;
        $user_info->email = $request->email;
        $user_info->password = bcrypt($request->password);
        $info->save();
        $user_info->save();
        return redirect('/settings');
    }
    public function reset_day()
    {
        $studnets = \App\Student::all();
        $workers = \App\Worker::all();
        foreach ($studnets as $student)
        {
            $student->is_present = 0;
            $student->save();
        }
        foreach ($workers as $worker)
        {
            $worker->is_present = 0;
            $worker->save();
        }
        return back();
    }
    public function reset_week()
    {
        $studnets = \App\Student::all();
        $workers = \App\Worker::all();
        foreach ($studnets as $student)
        {
            $student->visits_per_week = 0;
            $student->save();
        }
        foreach ($workers as $worker)
        {
            $worker->visits_per_week = 0;
            $worker->save();
        }
        return back();
    }
    public function reset_month()
    {
        $studnets = \App\Student::all();
        $workers = \App\Worker::all();
        foreach ($studnets as $student)
        {
            $student->visits_per_month = 0;
            $student->save();
        }
        foreach ($workers as $worker)
        {
            $worker->visits_per_month = 0;
            $worker->save();
        }
        return back();
    }
}
