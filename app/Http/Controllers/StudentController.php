<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
class StudentController extends Controller
{
    /* 
    |---------------------------------------------------------------------------
    | Метод: Create
    |---------------------------------------------------------------------------
    | Возвращает форму создания профиля ребёнка.
    |
    */

    public function create()
    {
        return view('students/create');
    }

    /* 
    |---------------------------------------------------------------------------
    | Метод: Store
    |---------------------------------------------------------------------------
    | Делает запрос на создание профиля ребёнка в базе данных.
    | При прохождении валидации создаётся профиль ребёнка.
    |
    */

    public function store(Request $request)
    {
        

        if ($request->method('post')) {

            /* 
            |---------------------------------------------------------------------------
            | Валидация: Создание
            |---------------------------------------------------------------------------
            |
            */

            $rules =[
                'name' => 'required|max:191|min:2',
                'middlename' => 'required|max:191|min:2',
                'surname' => 'required|max:191|min:2',
                'class' => 'required|max:191',
                'guide' => 'required|max:191|min:5',
                'date_of_birth' => 'required|max:191|date',
                'photo' => 'required',
                
            ];
            $messages = [
                'name.required' => 'Вы не ввели поле: Имя',
                'name.min' => 'Имя должно стостоять не менее, чем из 2х символов',

                'middlename.required' => 'Вы не ввели поле: Отчество',
                'middlename.min' => 'Отчество должно стостоять не менее, чем из 2х символов',

                'surname.required' => 'Вы не ввели поле: Фамилия',
                'surname.min' => 'Фамилия должна состоять не менее чем из двух символов',

                'class.required' => 'Вы не ввели поле: Класс',

                'guide.required' => 'Вы не ввели поле: Классный руководитель',
                'guide.min' => 'ФИО Классного руководителя должно состоять не менее чем из 5 символов',

                'date_of_birth.required' => 'Вы не ввели поле: Дата рождения',
                'date' => 'Не правильный формат даты. 1999-12-31',

                'photo.required' => 'Вы не загрузили фото'
            ];
            $this->validate($request,$rules, $messages);
            if($request->hasFile('photo')) {
                $file = $request->file('photo');
                $name = $request->file('photo')->getClientOriginalName();
                $file->move(public_path() . '/path', $name);
            }
        }
        $student = new \App\Student();
        $student->name = $request->name;
        $student->middlename = $request->middlename;
        $student->surname = $request->surname;
        $student->class = $request->class;
        $student->guide = $request->guide;
        $student->email = $request->email;
        $student->date_of_birth = $request->date_of_birth;
        $student->is_present = 0;
        $student->visits_per_week = 0;
        $student->visits_per_month = 0;
        $student->image = "/path/$name";
        $student->save();
        return redirect('student/create');
    }

    /* 
    |---------------------------------------------------------------------------
    | Метод: Edit
    |---------------------------------------------------------------------------
    | Возвращает форму изменения информации о ребёнке.
    |
    */

    public function edit($id)
    {
        $info = \App\Student::where('id', $id)->firstOrFail();
        // return dump(public_path().'\\'.$info->image);
        return view('students/edit', compact('info'));
    }

    /* 
    |---------------------------------------------------------------------------
    | Метод: Update
    |---------------------------------------------------------------------------
    | Обновляет информацию о ребёнке.
    | При прохождении валидации обновляются данные профиля ребёнка.
    |
    */

    public function update(Request $request)
    {
        if ($request->method('post')) {

            /* 
            |---------------------------------------------------------------------------
            | Валидация: Обновление
            |---------------------------------------------------------------------------
            |
            */

            $rules =[
                'name' => 'required|max:191|min:2',
                'middlename' => 'required|max:191|min:2',
                'surname' => 'required|max:191|min:2',
                'class' => 'required|max:191',
                'guide' => 'required|max:191|min:5',
                'date_of_birth' => 'required|max:191|date',
                'visits_per_week' => 'required|max:191',
                'visits_per_month' => 'required|max:191',
                
            ];
            $messages = [
                'name.required' => 'Вы не заполнили поле: Имя',
                'name.min' => 'Имя должно стостоять не менее, чем из 2х символов',

                'middlename.required' => 'Вы не заполнили поле: Отчество',
                'middlename.min' => 'Отчество должно стостоять не менее, чем из 2х символов',

                'surname.required' => 'Вы не заполнили поле: Фамилия',
                'surname.min' => 'Фамилия должна состоять не менее чем из двух символов',

                'class.required' => 'Вы не заполнили поле: Класс',

                'guide.required' => 'Вы не заполнили поле: Классный руководитель',
                'guide.min' => 'ФИО Классного руководителя должно состоять не менее чем из 5 символов',

                'date_of_birth.required' => 'Вы не заполнили поле: Дата рождения',
                'date' => 'Не правильный формат даты. 1999-12-31',

                'visits_per_week.required' => 'Вы не заполнили поле: Посещение за неделю',
                'visits_per_month.required' => 'Вы не заполнили поле: Посещение за месяц',

            ];
            $this->validate($request,$rules, $messages);
        }
        $student = \App\Student::find($request->id);
        $student->name = $request->name;
        $student->middlename = $request->middlename;
        $student->surname = $request->surname;
        $student->class = $request->class;
        $student->guide = $request->guide;
        $student->email = $request->email;
        $student->date_of_birth = $request->date_of_birth;
        $student->is_present = 0;
        $student->visits_per_week = $request->visits_per_week;
        $student->visits_per_month = $request->visits_per_month;
        $student->image = $request->file('photo');
        
        $student->save();
        return redirect("student/$request->id/edit");
    }

    /* 
    |---------------------------------------------------------------------------
    | Метод: Delete
    |---------------------------------------------------------------------------
    | Удаляет информацию о ребёнке.
    |
    */

    public function delete(Request $request)
    {
        $id = $request->input('student_id');
        $student_info = \App\Student::find($id);
        $class = $student_info->class;
        \App\Student::where('id', $id)->delete();
        return redirect("/class/$class");
    }
}
