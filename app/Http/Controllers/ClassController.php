<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClassController extends Controller
{
    public function index()
    {
        $student_model = \App\Student::paginate(15);
        $classes = []; $guides = []; $guides_class = [];
        foreach ($student_model as $class_info)
        {
            if (!in_array($class_info->class, $classes) and !in_array($class_info->guide, $guides_class))
            {
                $guides_class[] = $class_info->class;
                $classes[] = $class_info->class; 
                $guides[] = $class_info->guide;
            }
        }
        return view('classes/index', compact('classes', 'guides', 'student_model'));
    }
    public function delete(Request $request)
    {
        $class = $request->input('class_name');
        \App\Student::where('class', $class)->delete();
        return redirect('/classes');
    }
        
    public function list($number)
    {
        $students = \App\Student::where('class', $number)->paginate(15);
        return view('classes/info', compact('students', 'number'));
    }
}
