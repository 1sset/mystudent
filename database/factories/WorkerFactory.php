<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Worker;
use Faker\Generator as Faker;

$factory->define(Worker::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'middlename' => $faker->lastName,
        'surname' => $faker->lastName,
        'class' => rand(1, 10).Str::random(1),
        'email' => $faker->unique()->safeEmail,
        'date_of_birth' => $faker->date,
        'is_present' => 0,
        'visits_per_week' => 0,
        'visits_per_month' => 0,
    ];
});
