@extends('app')
@section('content')
<div class="card">
    <div class="card-header d-flex align-items-center">
        <h3 class="h4">Список рабочего персонала</h3>
    </div>
    <div class="card-body">
        <a href="/worker/create" class="btn btn-primary btn-block m-3">Создать профиль работника</a>
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                    <tr>
                        <th role="columnheader">Id</th>
                        <th role="columnheader">ФИО</th>
                        <th role="columnheader">Класс</th>
                        <th role="columnheader">Email</th>
                        <th role="columnheader">Д.Р</th>
                        <th role="columnheader">Присутствует</th>
                        <th role="columnheader">П.з.н</th>
                        <th role="columnheader">П.з.м</th>
                        <th role="columnheader">Удалить</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($workers as $worker)
                    <tr>
                        <th><a href="/worker/{{ $worker->id }}/edit">{{ $worker->id }}</a></th>
                        <th>{{ $worker->name }} {{ $worker->surname }} {{ $worker->middlename }}</th>
                        <th><a href="/class/{{ $worker->class }}">{{ $worker->class }}</a></th>
                        <th>{{ $worker->email }}</th>
                        <th>{{ $worker->date_of_birth }}</th>
                        <th>{{ $worker->is_present }}</th>
                        <th>{{ $worker->visits_per_week }}</th>
                        <th>{{ $worker->visits_per_month }}</th>
                        <th>
                            <a href="/worker/{{ $worker->id }}/delete" class="btn btn-sm btn-danger">Удалить</a>
                        </th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $workers->links() }}
        </div>

    </div>
</div>
@endsection