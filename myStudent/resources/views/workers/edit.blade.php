@extends('app')
@section('content')
<div class="card">
    <div class="card-header d-flex align-items-center">
        <h3 class="h4">Изменение профиля ребёнка</h3>
    </div>
    <div class="card-body">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li> {{ $error }} </li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="/worker/{{ $info->id }}" class="form-horizontal" method="POST">
            {{ csrf_field() }}
            {{ method_field('patch') }}
            <input type="hidden" name="id" value="{{ $info->id }}">
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="surname">Фамилия</label>
                        <input id="surname" type="text" value="{{ $info->surname }}" class="mr-3 form-control"
                            name="surname">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="name">Имя</label>
                        <input id="name" type="text" value="{{ $info->name }}" class="mr-3 form-control" name="name">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="middlename">Отчество</label>
                        <input id="middlename" type="text" value="{{ $info->middlename }}"" class=" mr-3 form-control"
                            name="middlename">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="class">Класс</label>
                        <input id="class" type="text" value="{{ $info->class }}" class="mr-3 form-control" name="class">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input id="email" type="text" value="{{ $info->email }}" class="mr-3 form-control" name="email">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="date_of_birth">Дата рождения</label>
                <input id="date_of_birth" type="text" value="{{ $info->date_of_birth }}" class="mr-3 form-control"
                    name="date_of_birth">
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="date_of_birth">Посещений в неделю</label>
                        <input type="text" name="visits_per_week" value="{{ $info->visits_per_week }}"
                            class="mr-3 form-control">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="date_of_birth">Посещений в месяц</label>
                        <input type="text" name="visits_per_month" value="{{ $info->visits_per_month }}"
                            class="mr-3 form-control">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Подтвердить</button>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <a href="/class/{{ $info->class }}" class="btn btn-danger">&#8592;</a>
                    </div>
                </div>
            </div>


        </form>
    </div>
</div>
@endsection