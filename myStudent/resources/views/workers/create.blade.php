@extends('app')
@section('content')
<div class="card">
    <div class="card-header d-flex align-items-center">
        <h3 class="h4">Создание профиля рабочего</h3>
    </div>
    <div class="card-body">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form action="/worker/store" class="form-horizontal" method="POST">
            @csrf
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="surname">Фамилия</label>
                        <input id="surname" type="text" placeholder="ИВАНОВ" class="mr-3 form-control" name="surname"
                            value="{{ old('surname') }}">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="name">Имя</label>
                        <input id="name" type="text" placeholder="ИВАН" class="mr-3 form-control" name="name"
                            value="{{ old('name') }}">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="middlename">Отчество</label>
                        <input id="middlename" type="text" placeholder="ИВАНОВИЧ" class="mr-3 form-control"
                            name="middlename" value="{{ old('middlename') }}">
                    </div>
                </div>
            </div>
            <div class=" row">
                <div class="col">
                    <div class="form-group">
                        <label for="class">Класс</label>
                        <input id="class" type="text" placeholder="1Б" class="mr-3 form-control" name="class" value="{{ old('class') }}">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input id="email" type="text" placeholder="exapmle@gmail.com" class="mr-3 form-control"
                            name="email" value="{{ old('email') }}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="date_of_birth">Дата рождения</label>
                <input id="date_of_birth" type="text" placeholder="1999-12-31" class="mr-3 form-control"
                    name="date_of_birth" value="{{ old('date_of_birth') }}">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Подтвердить</button>
            </div>
        </form>
    </div>
</div>
@endsection