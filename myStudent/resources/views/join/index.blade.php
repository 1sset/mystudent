@extends('app')
@section('content')
<div class="card">
  <div class="card-header d-flex align-items-center">
    <h3 class="h4">Пропуск</h3>
  </div>
</div>
@if(session('message'))
<div class="m-5 alert alert-success">{{ session('message') }}</div>
@endif
<div class="card-body">
  <form action="/join" method="post">
    @csrf
    <select name="type" class="form-control mb-3 shadow">
      <option value='student'>Ребёнок</option>
      <option value='worker'>Работник</option>
    </select>
    <div class="form-group">
      <input type="text" id="id" name='id' placeholder="id" class="mr-3 form-control shadow">
    </div>
    <button type="submit" class="btn btn-primary">Поиск</button>
  </form>
  @isset($info)

  <div class="m-5 text-center">
    <img src="{{ asset($info->image)}}" alt="" width=600 class="img-fluid rounded shadow">
  </div>
  <form action="/join/complete" method="post">
    @csrf
    <input type="hidden" name="id_complete" value="{{ $info->id }}">
    <input type="hidden" name="type_complete" value="{{ $type }}">
    <button type="submit" class="btn btn-sm btn-success">Подтвердить</button>
    <a href="/join" class="btn btn-sm btn-danger">Отклонить</a>
  </form>
  <div class="table-responsive m-5">
    <table class="table">
      <thead>
        <tr>
          <th>#</th>
          <th>ФИО</th>
          <th>Класс</th>
          <th>Присутствует</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">{{ $info->id }}</th>
          <td>{{ $info->surname }} {{ $info->name }} {{ $info->middlename }}</td>
          <td>{{ $info->class }}</td>
          <td>{{ $info->is_present }}</td>
        </tr>
      </tbody>
    </table>

  </div>

  @endisset
</div>
@endsection