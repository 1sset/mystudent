@extends('app')
@section('content')
<div class="card">
    <div class="card-header d-flex align-items-center">
        <h3 class="h4">{{ $number }}</h3>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                    <tr>
                        <th role="columnheader">Id</th>
                        <th role="columnheader">ФИО</th>
                        <th role="columnheader">Email</th>
                        <th role="columnheader">Д.Р</th>
                        <th role="columnheader">Присутствует</th>
                        <th role="columnheader">П.з.н</th>
                        <th role="columnheader">П.з.м</th>
                        <th role="columnheader">Удалить</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($students as $student)
                        <tr>
                            <th><a href="/student/{{ $student->id }}/edit">{{ $student->id }}</a></th>
                            <th>{{ $student->name }} {{ $student->surname }} {{ $student->middlename }}</th>
                            <th>{{ $student->email }}</th>
                            <th>{{ $student->date_of_birth }}</th>
                            <th>{{ $student->is_present }}</th>
                            <th>{{ $student->visits_per_week }}</th>
                            <th>{{ $student->visits_per_month }}</th>
                            <th>
                                <form action="/student/{{ $number }}/delete" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                    <input type="hidden" name="student_id" value="{{ $student->id }}">
                                    <button type="submit" class="btn btn-sm btn-danger">&#10006; Удалить</button>
                                </form>
                            </th>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $students->links() }}
        </div>
    </div>
</div>
@endsection