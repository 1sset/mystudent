@extends('app')
@section('content')
<div class="card">
    <div class="card-header d-flex align-items-center">
        <div class="row">
            <div class="col">
                <h3 class="h4">Список классов</h3>
            </div>
            <div class="col">
                <div class="alert alert-warning shadow" role="alert">
                    Что бы появился класс в списке, в нём должен состоять минимум 1 ребёнок!
                </div>
            </div>

        </div>


    </div>
    <div class="card-body">
        <a href="/student/create" class="btn btn-primary btn-block m-3">Создать профиль ребёнка</a>
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                    <tr>
                        <th role="columnheader">Класс</th>
                        <th role="columnheader">Руководство</th>
                        <th role="columnheader">Действие</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach (array_combine($classes, $guides) as $class => $guide)
                    <tr>
                        <th><a href="/class/{{ $class }}">{{ $class }}</a></th>
                        <th>{{ $guide }}</th>
                        <th>
                            <form action="/classes" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <input type="hidden" name="class_name" value="{{ $class }}">
                                <button type="submit" class="btn btn-sm btn-danger">&#10006; Удалить</button>
                            </form>
                        </th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $student_model->links() }}
        </div>
    </div>
</div>
@endsection