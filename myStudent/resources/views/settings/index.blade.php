@extends('app')
@section('content')
<div class="card">
    <div class="card-header d-flex align-items-center">
        <h3 class="h4">Настройки</h3>
    </div>
</div>
<div class="card-body">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form action="/settings/update" class="form-horizontal" method="POST">
        @csrf
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="name">Название учебного заведения</label>
                    <input id="name" type="text" class="mr-3 form-control shadow" name="name" value="{{ $info->name }}">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="email">Почта</label>
                    <input id="email" type="email" class="mr-3 form-control shadow" name="email"
                        value="{{ $user->email }}">
                </div>
            </div>
            <div class="col">
                <div class="form-group">
                    <label for="password">Пароль от почты</label>
                    <input id="password" type="password" class="mr-3 form-control shadow" name="password">
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Подтвердить</button>
        </div>
    </form>
    <div class="container m-5">
        <h3>Сброс посещений за промежутки:</h3>
        <hr />
        <div class="row">
            <form action="/settings/reset/day" method="POST">
                @csrf
                <button type="submit" class="btn btn-danger m-1">День</button>
            </form>

            <form action="/settings/reset/week" method="POST">
                @csrf
                <button type="submit" class="btn btn-danger m-1">Неделя</button>
            </form>

            <form action="/settings/reset/month" method="POST">
                @csrf
                <button type="submit" class="btn btn-danger m-1">Месяц</button>
            </form>
        </div>
    </div>
</div>
@endsection