<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JoinController extends Controller
{
    public function index()
    {
        return view('join/index');
    }
    public function get(Request $request)
    {
        if ($request->method('post'))
        {
            $rules = [
                'type' => 'required',
                'id' => 'required',
            ];
            $messages = [
                'type.required' => 'Поле: "Тип" не заполнено',
                'id.required' => 'Поле: "ID" не заполнено',
            ];
            $this->validate($request, $rules, $messages);
        }
        if ($request->type == "student")
        {
            $info = \App\Student::findOrFail($request->id);
            $type = 'student';
        } elseif ($request->type == "worker")
        {
            $info = \App\Worker::findOrFail($request->id);
            $type = 'worker';
        }
        return view('join/index', compact('info', 'type'));
    }
    public function complete(Request $request)
    {
        if ($request->type_complete == "student")
        {
            $student = \App\Student::find($request->id_complete);
            $student->is_present = 1;
            $student->visits_per_week++;
            $student->visits_per_month++;
            $student->save();
        } elseif ($request->type_complete == "worker")
        {   
            $worker = \App\Worker::find($request->id_complete);
            $worker->is_present = 1;
            $worker->visits_per_week++;
            $worker->visits_per_month++;
            $worker->save();
        }
        return redirect('/join')->with('message', 'Все прошло удачно');
    }
}
