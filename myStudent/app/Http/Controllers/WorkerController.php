<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WorkerController extends Controller
{
    public function index()
    {
        $workers = \App\Worker::paginate(15);
        return view('workers/index', compact('workers'));
    }
    /* 
    |---------------------------------------------------------------------------
    | Метод: Create
    |---------------------------------------------------------------------------
    | Возвращает форму создания профиля ребёнка.
    |
    */

    public function create()
    {
        return view('workers/create');
    }

    /* 
    |---------------------------------------------------------------------------
    | Метод: Store
    |---------------------------------------------------------------------------
    | Делает запрос на создание профиля ребёнка в базе данных.
    | При прохождении валидации создаётся профиль ребёнка.
    |
    */

    public function store(Request $request)
    {
        if ($request->method('post')) {

            /* 
            |---------------------------------------------------------------------------
            | Валидация: Создание
            |---------------------------------------------------------------------------
            |
            */

            $rules =[
                'name' => 'required|max:191|min:2',
                'middlename' => 'required|max:191|min:2',
                'surname' => 'required|max:191|min:2',
                'class' => 'required|max:191',
                'date_of_birth' => 'required|max:191|date',
                
            ];
            $messages = [
                'name.required' => 'Вы не ввели поле: Имя',
                'name.min' => 'Имя должно стостоять не менее, чем из 2х символов',

                'middlename.required' => 'Вы не ввели поле: Отчество',
                'middlename.min' => 'Отчество должно стостоять не менее, чем из 2х символов',

                'surname.required' => 'Вы не ввели поле: Фамилия',
                'surname.min' => 'Фамилия должна состоять не менее чем из двух символов',

                'class.required' => 'Вы не ввели поле: Класс',

                'date_of_birth.required' => 'Вы не ввели поле: Дата рождения',
                'date' => 'Не правильный формат даты. 1999-12-31',

            ];
            $this->validate($request,$rules, $messages);
        }
        
        $worker = new \App\Worker();
        $worker->name = $request->name;
        $worker->middlename = $request->middlename;
        $worker->surname = $request->surname;
        $worker->class = $request->class;
        $worker->email = $request->email;
        $worker->date_of_birth = $request->date_of_birth;
        $worker->is_present = 0;
        $worker->visits_per_week = 0;
        $worker->visits_per_month = 0;
        $worker->save();
        return redirect('worker/create');
    }

    /* 
    |---------------------------------------------------------------------------
    | Метод: Edit
    |---------------------------------------------------------------------------
    | Возвращает форму изменения информации о ребёнке.
    |
    */

    public function edit($id)
    {
        $info = \App\Worker::where('id', $id)->firstOrFail();
        return view('workers/edit', compact('info'));
    }

    /* 
    |---------------------------------------------------------------------------
    | Метод: Update
    |---------------------------------------------------------------------------
    | Обновляет информацию о ребёнке.
    | При прохождении валидации обновляются данные профиля ребёнка.
    |
    */

    public function update(Request $request)
    {
        if ($request->method('post')) {

            /* 
            |---------------------------------------------------------------------------
            | Валидация: Обновление
            |---------------------------------------------------------------------------
            |
            */

            $rules =[
                'name' => 'required|max:191|min:2',
                'middlename' => 'required|max:191|min:2',
                'surname' => 'required|max:191|min:2',
                'class' => 'required|max:191',
                'date_of_birth' => 'required|max:191|date',
                'visits_per_week' => 'required|max:191',
                'visits_per_month' => 'required|max:191',
                
            ];
            $messages = [
                'name.required' => 'Вы не заполнили поле: Имя',
                'name.min' => 'Имя должно стостоять не менее, чем из 2х символов',

                'middlename.required' => 'Вы не заполнили поле: Отчество',
                'middlename.min' => 'Отчество должно стостоять не менее, чем из 2х символов',

                'surname.required' => 'Вы не заполнили поле: Фамилия',
                'surname.min' => 'Фамилия должна состоять не менее чем из двух символов',

                'class.required' => 'Вы не заполнили поле: Класс',

                'date_of_birth.required' => 'Вы не заполнили поле: Дата рождения',
                'date' => 'Не правильный формат даты. 1999-12-31',

                'visits_per_week.required' => 'Вы не заполнили поле: Посещение за неделю',
                'visits_per_month.required' => 'Вы не заполнили поле: Посещение за месяц',

            ];
            $this->validate($request,$rules, $messages);
        }
        $worker = \App\Worker::find($request->id);
        $worker->name = $request->name;
        $worker->middlename = $request->middlename;
        $worker->surname = $request->surname;
        $worker->class = $request->class;
        $worker->email = $request->email;
        $worker->date_of_birth = $request->date_of_birth;
        $worker->is_present = 0;
        $worker->visits_per_week = $request->visits_per_week;
        $worker->visits_per_month = $request->visits_per_month;
        $worker->save();
        return redirect("worker/$request->id/edit");
    }

    /* 
    |---------------------------------------------------------------------------
    | Метод: Delete
    |---------------------------------------------------------------------------
    | Удаляет информацию о ребёнке.
    |
    */

    public function delete($id)
    {
        \App\Worker::where('id', $id)->delete();
        return redirect("/workers");
    }
}
