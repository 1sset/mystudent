<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /* Для создания админки */
        $this->call(AdminSeeder::class);

        /* Для создания фейк-данных */
        // $this->call(StudentsTableSeeder::class);
        // $this->call(WorkersTableSeeder::class);
    }
}
