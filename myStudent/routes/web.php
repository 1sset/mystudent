<?php
Auth::routes(['register' => false]);
Route::get('/logout', 'DashboardController@logout');
Route::middleware('auth')->group(function () {
    #           Выводит главную страницу
    Route::get('/', 'DashboardController@index')->middleware('auth');
    #           Настройки
    Route::get('/settings', 'SettingController@index');
    Route::post('/settings/update', 'SettingController@update');
    Route::post('/settings/reset/day', 'SettingController@reset_day');
    Route::post('/settings/reset/week', 'SettingController@reset_week');
    Route::post('/settings/reset/month', 'SettingController@reset_month');

    /* 
    |---------------------------------------------------------------------------
    | ClassController маршруты
    |---------------------------------------------------------------------------
    | Метод, с помощью которого получаем данные о классах.
    |
    */

    Route::get('/classes', 'ClassController@index');
    Route::delete('/classes', 'ClassController@delete');
    Route::get('/class/{number}', 'ClassController@list');

    /* 
    |---------------------------------------------------------------------------
    | StudentController маршруты
    |---------------------------------------------------------------------------
    | Метод, с помощью которого получаем данные о детях.
    |
    */

    Route::get('/student/create', 'StudentController@create');
    Route::post('/student/store', 'StudentController@store');
    Route::get('/student/{id}/edit', 'StudentController@edit');
    Route::delete('/student/{id}/delete', 'StudentController@delete');
    Route::patch('/student/{id}', 'StudentController@update');

    /* 
    |---------------------------------------------------------------------------
    | WorkerController маршруты
    |---------------------------------------------------------------------------
    | Метод, с помощью которого получаем данные о рабочем персонале.
    |
    */

    Route::get('/workers', 'WorkerController@index');
    Route::get('/worker/create', 'WorkerController@create');
    Route::post('/worker/store', 'WorkerController@store');
    Route::get('/worker/{id}/edit', 'WorkerController@edit');
    Route::get('/worker/{id}/delete', 'WorkerController@delete');
    Route::patch('/worker/{id}', 'WorkerController@update');

    /* 
    |---------------------------------------------------------------------------
    | JoinController маршруты
    |---------------------------------------------------------------------------
    | Метод, с помощью которого засчитываются посещения.
    |
    */
    Route::post('/join/complete', 'JoinController@complete');
    Route::get('/join', 'JoinController@index');
    Route::post('/join', 'JoinController@get');
    
});



